# Couplant detection

## Purpose of this package

- The Arduino sketch is installed on a DFR0282 board (Arduino Leonardo compatible).  
  The board reads a digital signal and comunicates the value of this signal on a serial port.
- The serial port is monitored by another card on which linux and ROS are installed.
 So this information is published to a ROS topic.
  This is done by the software module talker.cpp 

## Hardware
- DFROBOT 282 (Arduino Leonardo compatible)


## Software
- Arduino IDE 1.8.x
- Ubuntu Mate Xenial (16.04)
- ROS Kinetic

## Comments

- The board is assumed to be found on /dev/ttyACM0


## Author

Ciro Capasso (ciro.capasso@innotecuk.com)