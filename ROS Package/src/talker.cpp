#include "ros/ros.h"
#include "std_msgs/Int16.h"
#include "beginner_tutorials/wiringSerial.h"
#include <stdio.h>
#include <unistd.h>

#include <sstream>


int main(int argc, char **argv){

    ros::init(argc, argv, "talker");
    ros::NodeHandle n;
    ros::Publisher chatter_pub = n.advertise<std_msgs::Int16>("chatter", 10);
    ros::Rate loop_rate(10);

    int fd = serialOpen("/dev/ttyACM0",9600);
    int receivedValue;

    while (ros::ok()){

        std_msgs::Int16 msg;
        receivedValue = serialGetchar(fd);
        msg.data = (short int)receivedValue;
        msg.data = msg.data - 48;

        ROS_INFO("%d", msg.data); printf(" %c ",receivedValue);

        chatter_pub.publish(msg);
        ros::spinOnce();
        loop_rate.sleep();

    }

    serialClose(fd);

    return 0;
}
